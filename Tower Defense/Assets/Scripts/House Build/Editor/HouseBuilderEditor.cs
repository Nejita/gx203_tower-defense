using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HouseBuilder))]
public class HouseBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        HouseBuilder houseBuilder = (HouseBuilder)target;

        if (GUILayout.Button("Build!"))
        {
            houseBuilder.BuildWall();
        }
    }
}
