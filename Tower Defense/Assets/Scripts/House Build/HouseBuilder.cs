using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseBuilder : MonoBehaviour
{
    [SerializeField] private GameObject brick;
    [SerializeField] private GameObject builder; //where to sort spawned objects
    [SerializeField] float xOffSet;
    [SerializeField] float yOffSet;
    [SerializeField] float zOffSet;
    [SerializeField] int xAmount;
    [SerializeField] int yAmount;
    [SerializeField] int zAmount;

    public void BuildWall()
    {

        // parameters to repeat brick
        for (float xPos = 0; xPos < xAmount; xPos += xOffSet)
        {
            for (float yPos = 0; yPos > -yAmount; yPos -= yOffSet)
            {
                for (float zPos = 0; zPos > -zAmount; zPos -= zOffSet)
                { //repeats brick across level
                    var newBrick = Instantiate(brick, new Vector3(xPos, yPos, zPos), brick.transform.rotation);
                    newBrick.transform.parent = builder.transform; //object instantiated as child of "builder"
                }
            }
        }
        Debug.Log("building...");
    }
}