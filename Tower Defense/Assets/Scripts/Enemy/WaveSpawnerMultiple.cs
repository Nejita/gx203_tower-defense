using System.Collections;
using TMPro;
using UnityEngine;

public class WaveSpawnerMultiple : MonoBehaviour
{
    // wave spawner for multiple spawn points and enemies

    public enum SpawnState { Spawing, Waiting, Counting }

    [System.Serializable]
    public class Wave
    {
        public string name; //name of wave
        public Transform enemy; //enemy prefab
        public int count;
        public float spawnRate;
    }

    public Wave [] waves; //waves list
    public Transform [] spawnPoints;
    public TextMeshProUGUI WaveCountdownTxt;
    public TextMeshProUGUI WavecompletedTxt;
    public float timeBetweenWaves = 5f; //time between waves
    public float countdown; //countdown between wave spawns
    [SerializeField] SpawnState state = SpawnState.Counting;
    [SerializeField] float timeBetweenEnemies = 0.3f; //time difference between enemies in the same wave to spawn
    [SerializeField] GameObject waveCountdown;
    public Transform enemiesSpawnedFolder; //where to sort spawned enemies under on the hierarchy window
    public Transform enemiesPoolFolder; //where to sort pooled enemies under on the hierarchy window
    private int waveIndex = 0; //number of additional enemies in wave (0 = Default starting amount of enemies is 1)
    private float searchCountdown = 1; //number of additional enemies in wave (0 = Default starting amount of enemies is 1)
    public int wavesCompleted;
    public int increaseEnemyDamage = 20;
    public int increaseEnemyHealth = 50;
    public int enemyPoolCount;
    private GameObject newEnemy;


    private void Start()
    {
        countdown = timeBetweenWaves; //set countdown time to equal the time between waves

        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        enemyPoolCount = enemiesPoolFolder.childCount;
    }

    private void Update()
    {
        if (state == SpawnState.Waiting)
        {
            //if enemies are all dead wave is completed 
            if (!EnemyIsAlive())
            {
                WaveCompleted();
            }

            else
            {
                return;
            }
        }
        // CheckEnemyStatus();
        Countdown();
    }

    void Countdown()
    {
        if (countdown <= 0)
        {
            if (state != SpawnState.Spawing) //if not spawning wave 
            {
                //start wave spawning
                StartCoroutine(SpawnWave(waves [waveIndex]));

            }
            waveCountdown.SetActive(false);
        }

        else
        {
            countdown -= Time.deltaTime; // decrease countdown over time
            if (countdown < 0) //stops from going into negative values
            {
                countdown = 0;
            }
            WaveCountdownTxt.text = Mathf.Floor(countdown).ToString();

        }
    }

    void WaveCompleted()
    {
        Debug.Log("Wave complete");

        state = SpawnState.Counting;
        countdown = timeBetweenWaves; //reset countdown
        waveCountdown.SetActive(true);
        wavesCompleted++;//amount used for high score
        WavecompletedTxt.text = Mathf.Floor(wavesCompleted).ToString();

        if (waveIndex + 1 > waves.Length - 1)
        {
            waveIndex = 0;
            Debug.Log("All waves completed! Looping");
            //when all waves are completed and starts new loop, increase enemy difficulty (damage amount, health)
            Enemy.damageGiven = Enemy.damageGiven + increaseEnemyDamage;
            Enemy.startHealth = Enemy.startHealth + increaseEnemyHealth;

        }
        else
        {
            waveIndex++; //increase amount of enemies every wave
        }
    }

    bool EnemyIsAlive()
    {
        //checks if enemies are still alive every 1 second
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave" + _wave.name);
        state = SpawnState.Spawing;
        //spawn
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.spawnRate); //wait time for enemies within same wave
        }

        state = SpawnState.Waiting;
        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        //spawn enemy using object pooling
        Debug.Log("Spawning Enemy" + _enemy.name);
        enemyPoolCount = enemiesPoolFolder.childCount; 
        newEnemy = enemiesPoolFolder.GetChild(Random.Range(0, enemyPoolCount)).gameObject; // get enemy from enemy pool
        newEnemy.transform.SetParent(enemiesSpawnedFolder);
        Transform _sp = spawnPoints [Random.Range(0, spawnPoints.Length)]; //spawn point for enemy is random
        newEnemy.transform.position = new Vector3 (_sp.position.x, 0, _sp.position.z);
        Enemy.health = Enemy.startHealth;
        newEnemy.SetActive(true);//set enemy to active
    }

}