using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public Transform tower;
    public NavMeshAgent agent;
    public WaveSpawnerMultiple waveSpawnerMultipleScript;
    public AudioClip deafeatSound;
    public AudioSource defeat;
    public Image healthBar;

    public static float startHealth = 100f;
    public static float health;

    public static bool isDead = false;
    public static int damageGiven = 20;

    private void Start()
    {
        agent = this.transform.GetComponent<NavMeshAgent>();
        isDead = false;
        defeat = GetComponent<AudioSource>();
        health = startHealth;
    }

    private void Update()
    {
        Move();
    }

    public void TakeDamage(int amount)
    {
        health -= amount;

        healthBar.fillAmount = health/startHealth;

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        defeat.PlayOneShot(deafeatSound);
        this.gameObject.SetActive(false);
        this.gameObject.transform.SetParent(waveSpawnerMultipleScript.enemiesPoolFolder);
        isDead = true;
        health = startHealth;
    }

    void Move()
    {
        //if there's a tower set that as the location to go to.

        if (tower)
        {
            agent.SetDestination(tower.position);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Tower"))
        {
            health = startHealth;
            Destroy(gameObject);
        }

    }
}