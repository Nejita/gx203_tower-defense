using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] GameObject _inGamePauseMenu;
    public AudioSource main;
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResumeGame()
    {
        _inGamePauseMenu.SetActive(false);
        Time.timeScale = 1;
        main.Play();

    }

    public void PauseMenu()
    {

        _inGamePauseMenu.SetActive(true);
        Time.timeScale = 0;
        main.Stop();

    }
}