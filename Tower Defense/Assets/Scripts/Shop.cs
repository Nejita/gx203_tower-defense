using TMPro;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint catapultTurret;

    public TurretBlueprint archerTurret;
    public TowerHealth towerHealthScript;
    BuildManager buildManager;
    public TextMeshProUGUI catapultMoneyPriceTxt;
    public TextMeshProUGUI catapultWoodPriceTxt;
    public TextMeshProUGUI catapultIronPriceTxt;
    public TextMeshProUGUI archerMoneyPriceTxt;
    public TextMeshProUGUI archerWoodPriceTxt;
    public TextMeshProUGUI archerIronPriceTxt;
    public Color canBuy;
    public Color cannotBuy;

    void Start()
    {
        buildManager = BuildManager.instance;
        //price text =  price set
        catapultMoneyPriceTxt.text = "$" + catapultTurret.price.ToString();
        catapultWoodPriceTxt.text = "Wood: " + catapultTurret.wood.ToString();
        catapultIronPriceTxt.text = "Iron: " + catapultTurret.iron.ToString();
        archerMoneyPriceTxt.text = "$" + archerTurret.price.ToString();
        archerWoodPriceTxt.text = "Wood: " + archerTurret.wood.ToString();
        archerIronPriceTxt.text = "Iron: " + archerTurret.iron.ToString();
    }

    private void FixedUpdate()
    {
        CanBuyColor();
    }

    public void CanBuyColor()//sets colour feedback for store based on available resources
    {
        if (PlayerStats.money < catapultTurret.price || PlayerStats.wood < catapultTurret.wood || PlayerStats.iron < catapultTurret.iron)
        {
            catapultMoneyPriceTxt.color = cannotBuy;
            catapultWoodPriceTxt.color = cannotBuy;
            catapultIronPriceTxt.color = cannotBuy;
        }

        else if(PlayerStats.money >= catapultTurret.price && PlayerStats.wood >= catapultTurret.wood && PlayerStats.iron >= catapultTurret.iron)
        {
            catapultMoneyPriceTxt.color = canBuy;
            catapultWoodPriceTxt.color = canBuy;
            catapultIronPriceTxt.color = canBuy;
        }

        if (PlayerStats.money < archerTurret.price || PlayerStats.wood < archerTurret.wood || PlayerStats.iron < archerTurret.iron)
        {
            archerMoneyPriceTxt.color = cannotBuy;
            archerWoodPriceTxt.color = cannotBuy;
            archerIronPriceTxt.color = cannotBuy;
        }

        else if(PlayerStats.money >= archerTurret.price && PlayerStats.wood >= archerTurret.wood && PlayerStats.iron >= archerTurret.iron)
        {
            archerMoneyPriceTxt.color = canBuy;
            archerWoodPriceTxt.color = canBuy;
            archerIronPriceTxt.color = canBuy;
        }

        if (PlayerStats.money < towerHealthScript.towerUpgradeCost || PlayerStats.wood < towerHealthScript.towerUpgradeWood || PlayerStats.iron < towerHealthScript.towerUpgradeIron)
        {
            towerHealthScript.upgradePriceTxt.color = cannotBuy;
            towerHealthScript.ironUpgradePriceTxt.color = cannotBuy;
            towerHealthScript.woodUpgradePriceTxt.color = cannotBuy;
        }

        else if(PlayerStats.money >= towerHealthScript.towerUpgradeCost && PlayerStats.wood >= towerHealthScript.towerUpgradeWood && PlayerStats.iron >= towerHealthScript.towerUpgradeIron)
        {
            towerHealthScript.upgradePriceTxt.color = canBuy;
            towerHealthScript.ironUpgradePriceTxt.color = canBuy;
            towerHealthScript.woodUpgradePriceTxt.color = canBuy;
        }
    }

    public void TowerUpgrade()
    {
        towerHealthScript.UpgradeTower();
        towerHealthScript.towerLvlTxt.text = "Tower Level: " + towerHealthScript.towerUpgradeLvl.ToString();
        towerHealthScript.upgradePriceTxt.text = "$" + towerHealthScript.towerUpgradeCost.ToString();
        towerHealthScript.ironUpgradePriceTxt.text = "Iron: " + towerHealthScript.towerUpgradeIron.ToString();
        towerHealthScript.woodUpgradePriceTxt.text = "Wood: " + towerHealthScript.towerUpgradeWood.ToString();
    }
    public void SelectCatapultTurret()
    {
        buildManager.SelectTurretToBuild(catapultTurret);
    }
    public void SelectArcherTurret()
    {
        buildManager.SelectTurretToBuild(archerTurret);
    }

}