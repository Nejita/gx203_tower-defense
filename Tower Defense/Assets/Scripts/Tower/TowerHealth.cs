using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TowerHealth : MonoBehaviour
{
    public float health;
    public float towerStartHealth = 100f;
    public int towerUpgradeHealth = 50;
    public int towerUpgradeCost = 1000;
    public int towerUpgradeWood = 1500;
    public int towerUpgradeIron = 1250;
    public int towerUpgradeLvl = 1;
    [SerializeField] Enemy Enemy;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] GameObject store;

     public TextMeshProUGUI ironUpgradePriceTxt;
     public TextMeshProUGUI upgradePriceTxt;
     public TextMeshProUGUI woodUpgradePriceTxt;
     public TextMeshProUGUI towerLvlTxt;

    public AudioClip loseSound;
    public AudioSource lose;
    public AudioSource main;
    public Image healthBar;
    void Start()
    {
        health = towerStartHealth;
        Enemy = Enemy.GetComponent<Enemy>();
        lose = GetComponent<AudioSource>();
        gameOverScreen.SetActive(false);
        store.SetActive(true);
        towerUpgradeLvl = 1;
        upgradePriceTxt.text = "$" + towerUpgradeCost.ToString();
        ironUpgradePriceTxt.text = "Iron: " + towerUpgradeIron.ToString();
        woodUpgradePriceTxt.text = "Wood: " + towerUpgradeWood.ToString();
        towerLvlTxt.text = "Tower Level: " + towerUpgradeLvl.ToString();

    }

    public void UpgradeTower()
    {
        if (PlayerStats.money >= towerUpgradeCost && PlayerStats.wood >= towerUpgradeWood && PlayerStats.iron >= towerUpgradeIron)
        {
            PlayerStats.money -= towerUpgradeCost;
            PlayerStats.wood -= towerUpgradeWood;
            PlayerStats.iron -= towerUpgradeIron;
            towerStartHealth = towerStartHealth + (towerUpgradeHealth * towerUpgradeLvl);//tower health upgrade amount increases with every level.
            health = towerStartHealth;
            towerUpgradeLvl = towerUpgradeLvl + 1;//add a new tower level
            towerUpgradeCost = towerUpgradeCost + (towerUpgradeCost * towerUpgradeLvl);
            towerUpgradeWood = towerUpgradeWood + (towerUpgradeWood * towerUpgradeLvl);
            towerUpgradeIron = towerUpgradeIron + (towerUpgradeIron * towerUpgradeLvl);        
        }

    }

    void Die()
    {
        GameOver();
        Debug.Log("Game over");
        lose.PlayOneShot(loseSound);
    }
    void GameOver()
    {
        Time.timeScale = 0;
        gameOverScreen.SetActive(true);
        store.SetActive(false);
        main.Stop();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            health -= Enemy.damageGiven;

            healthBar.fillAmount = health / towerStartHealth;

            if (health <= 0)
            {
                Die();
            }
        }

    }
}