using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerMovement : MonoBehaviour
{
    public float _speed = 5f;
    [SerializeField] List<Transform> _waypointList = new List<Transform>();

    private Transform _target;
    private Enemy enemyScript;
    private int _waypointIndex = 0;
    public Vector3 startPoint;
    public GameObject towerPos;

    private void Start()
    {
        _target = _waypointList [0]; //set target to first waypoint in index
        startPoint = transform.position;
        towerPos.transform.position = this.gameObject.transform.position;
    }

    private void Update()
    {
        if (_waypointIndex <= _waypointList.Count - 1)
        {
            Vector3 dir = _target.position - transform.position;
            transform.Translate(dir.normalized * _speed * Time.deltaTime, Space.World); //momentum towards waypoint index 0
            // transform.rotation = Quaternion.LookRotation(Vector3.forward, dir); //rotates tower to face direction traveling

        }

        if (Vector3.Distance(transform.position, _target.position) < +0.1f)//when this close to next waypoint location move to the next one
        {
            GetNextWayPoint();
        }

        else if (_waypointIndex > _waypointList.Count - 1)
        {
            ResetIndex();
        }
        towerPos.transform.position = this.gameObject.transform.position;

    }
    void GetNextWayPoint()
    {

        _target = _waypointList [_waypointIndex]; //set target to next waypoint on index
        _waypointIndex++;

    }

    void ResetIndex()
    {
        {
            _waypointIndex = 0;
        }
    }

    public void ResetPosition()
    {
        transform.position = startPoint;
        _target = _waypointList [0]; //set target to first waypoint in index
    }
}