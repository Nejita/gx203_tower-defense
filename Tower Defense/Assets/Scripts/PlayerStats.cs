using System.Collections;
using TMPro;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static int money;
    public static int wood;
    public static int iron;
    public int startMoney = 150;
    public int startWood = 200;
    public int startIron = 250;

    public TextMeshProUGUI moneyTxt;

    public TextMeshProUGUI woodTxt;

    public TextMeshProUGUI ironTxt;
    public TextMeshProUGUI healthTxt;
    

    public TowerHealth towerHealth;

    private void Start()
    {
        money = startMoney;
        moneyTxt.text = "$" + money.ToString();

        wood = startWood;
        woodTxt.text = "Wood: " + wood.ToString();

        iron = startWood;
        ironTxt.text = "Iron: " + iron.ToString();

    }

    private void Update()
    {
        moneyTxt.text = "$" + money.ToString();
        woodTxt.text = "Wood: " + wood.ToString();
        ironTxt.text = "Iron: " + iron.ToString();
        healthTxt.text = "HP: " + towerHealth.health.ToString() + " / " + towerHealth.towerStartHealth.ToString();
    }
}