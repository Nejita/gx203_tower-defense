using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ToolTip : MonoBehaviour
{
    public TextMeshProUGUI tipHeader;
    public TextMeshProUGUI tipContent;
    public LayoutElement layoutElement;
    public int characterWrapLimit;
    public RectTransform rectTransform;

    private void Awake() {
        rectTransform = GetComponent<RectTransform>();
    }

    public void SetText(string content, string header = "")
    {
        if (string.IsNullOrEmpty(header))
        {
            tipHeader.gameObject.SetActive(false);
        }
        else
        {
            tipHeader.gameObject.SetActive(true);
            tipHeader.text = header;
        }

        tipContent.text = content;
    }
    void Update()
    {
        if (Application.isEditor)
        {
            int headerLength = tipHeader.text.Length;
            int contentLength = tipContent.text.Length;

            layoutElement.enabled = (headerLength > characterWrapLimit || contentLength > characterWrapLimit) ? true : false;
        }

        Vector2 position = Input.mousePosition;

        float pivotX = position.x / Screen.width;
        float pivotY = position.y / Screen.height;

        rectTransform.pivot = new Vector2(pivotX, pivotY);
        transform.position = position;
    }
}