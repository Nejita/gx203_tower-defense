using UnityEngine;

public class Waypoints : MonoBehaviour
{
    //list for waypoints
    public static Transform [] waypoints;


    private void Awake()
    {
        waypoints = new Transform[transform.childCount]; //list made up of children stored inside waypoints game object
        for (int i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = transform.GetChild(i);
        }
    }
}