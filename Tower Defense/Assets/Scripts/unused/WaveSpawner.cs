using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    //Wave spawner or single spawn point
    public TextMeshProUGUI WaveCountdownTxt;
    public Transform enemyPrefab;
    public Transform spawnPoint;
    public float timeBetweenWaves = 5f;
    [SerializeField] float countdown = 1f; //how long it takes for first enemy to spawn
    [SerializeField] float timeBetweenEnemies = 0.3f; //time difference between enemies in the same wave to spawn
    [SerializeField] private GameObject enemiesFolder; //where to sort spawned enemies under on the hierarchy window
    private int waveIndex = 0; //number of additional enemies in wave (0 = Default starting amount of enemies is 1)

    private void Update()
    {
        Countdown();
    }

    void Countdown()
    {
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves; //reset countown so it doesn't fall below 0 and into negative numbers
        }

        countdown -= Time.deltaTime; // decrease countdown over time
        WaveCountdownTxt.text = Mathf.Floor(countdown).ToString();
    }

    IEnumerator SpawnWave()
    {
        Debug.Log("incoming");
        waveIndex++; //increase amount of enemies every wave
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(timeBetweenEnemies); //wait time for enemies within same wave
        }
    }

    void SpawnEnemy()
    {
        var newEnemy = Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation); //instantiate object at spawn location
        newEnemy.transform.parent = enemiesFolder.transform; //file newly spawned enemies into the enemies empty object folder
    }
}