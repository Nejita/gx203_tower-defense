using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;
    public int catapultPrice;

    void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than 1 BuildManager in scene");
            return;
        }
        instance = this;
    }

    public GameObject catapultTurretPrefab;
    public GameObject archerTurretPrefab;

    private TurretBlueprint turretToBuild;
    private Node selectedNode;
    public NodeUi nodeUi;

    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.money >= turretToBuild.price && PlayerStats.wood >= turretToBuild.wood && PlayerStats.iron >= turretToBuild.iron; } } // is true if player has the required resources

    public void SelectNode(Node node)
    {
        if (selectedNode == node)
        {
            DeselectNode();
            return;
        }
        selectedNode = node;
        turretToBuild = null;

        nodeUi.SetTargetNode(node);
    }

    public void DeselectNode()
    {
        selectedNode = null;
        nodeUi.HideUI();
    }

    public void SelectTurretToBuild(TurretBlueprint Turret)
    {
        turretToBuild = Turret;
        DeselectNode();
    }
    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }
}