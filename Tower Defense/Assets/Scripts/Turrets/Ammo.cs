using UnityEngine;

public class Ammo : MonoBehaviour
{
    [SerializeField] float speed = 20f;
    public float hitRadius = 0f;
    public GameObject impactEffect;
    public int defeatReward;
    public int damage = 20;
    BuildManager buildManager;
    Enemy enemy;
    private Transform target;

    public void Pursue(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
    }

    void HitTarget()
    {
        GameObject effectIns = (GameObject) Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 2f);

        if (hitRadius > 0)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }
        Destroy(gameObject);
        
        if (Enemy.isDead == true)
        {
            PlayerStats.money += defeatReward;//CASH INCOME
            PlayerStats.iron += Random.Range(50, 200);//IRON FOUND
            PlayerStats.wood +=  Random.Range(20, 100);//WOOD FOUND
            Enemy.isDead = false;
        }
    }

    void Damage(Transform enemy)
    {
        Enemy invader = enemy.GetComponent<Enemy>();

        invader.TakeDamage(damage);

    }

    void Explode()
    {
        Collider [] hitObject = Physics.OverlapSphere(transform.position, hitRadius);

        foreach (Collider collider in hitObject)
        {
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, hitRadius);
        Gizmos.color = Color.green;
    }
}