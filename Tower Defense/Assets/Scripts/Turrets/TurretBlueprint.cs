using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretBlueprint
{
    public GameObject prefab;
    public int price;
    public int wood;
    public int iron;
    public int sell;
    public int salvageWood;
    public int salvageIron;

    

    public GameObject upgradePrefab;
    public int upgradePrice;
    public int upgradeWood;
    public int upgradeIron;
    public int sellUpgradePrice;
    public int salvageUpgradeWood;
    public int salvageUpgradeIron;


}