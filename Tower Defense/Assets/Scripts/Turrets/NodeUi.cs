using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeUi : MonoBehaviour
{
    private Node target;
    public GameObject ui;
    public TurretBlueprint blueprint;
    BuildManager buildManager;
    public TextMeshProUGUI sellTxt;
    public TextMeshProUGUI salvageTxt;


    public void SetTargetNode(Node _target)
    {
        this.target = _target;

        transform.position = target.GetBuildPosition();
        ui.SetActive(true);

        buildManager = BuildManager.instance;
    }

    public void HideUI()
    {
        ui.SetActive(false);
    }

    public void Upgrade()
    {
        target.UpgradeTurret(target);
        BuildManager.instance.DeselectNode();
    }

    public void Sell()
    {
        target.SellTurret(target);
        BuildManager.instance.DeselectNode();
    }

    public void Salvage()
    {
        target.SalvageTurret(target);
        BuildManager.instance.DeselectNode();
    }

}