using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color canPlaceColour;
    public Color cannotPlaceColour;
    public Vector3 posOffSet;
    [Header("Optional")]
    public GameObject turret;
    public TurretBlueprint turretBlueprint;
    public bool isUpgraded = false;
    private Color startColour;
    private Renderer rend;

    BuildManager buildManager;
    public Shop shop;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        startColour = rend.material.color;

        buildManager = BuildManager.instance;
    }
    public Vector3 GetBuildPosition()
    {
        return transform.position + posOffSet;
    }

    void BuildTurret(TurretBlueprint blueprint)
    {
        if (PlayerStats.money < blueprint.price || PlayerStats.wood < blueprint.wood || PlayerStats.iron < blueprint.iron)
        {
            Debug.Log("no money");
            return;
        }

        PlayerStats.money -= blueprint.price;
        PlayerStats.wood -= blueprint.wood;
        PlayerStats.iron -= blueprint.iron;

        GameObject _turret = (GameObject) Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        turretBlueprint = blueprint;
    }
    public void UpgradeTurret(Node node)
    {

        if (PlayerStats.money < turretBlueprint.upgradePrice || PlayerStats.wood < turretBlueprint.upgradeWood || PlayerStats.iron < turretBlueprint.upgradeIron)
        {
            Debug.Log("no money, you can't upgrade");
            return;
        }

        PlayerStats.money -= turretBlueprint.upgradePrice;
        PlayerStats.wood -= turretBlueprint.upgradeWood;
        PlayerStats.iron -= turretBlueprint.upgradeIron;

        Destroy(turret); //destroy old turret
        //build new upgrade
        GameObject _turret = (GameObject) Instantiate(turretBlueprint.upgradePrefab, node.GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        isUpgraded = true;
    }
    public void SellTurret(Node node)
    {
        if (!isUpgraded)
        {
            PlayerStats.money += turretBlueprint.sell;
        }
        else if (isUpgraded)
        {
            PlayerStats.money += turretBlueprint.sellUpgradePrice;
        }

        Destroy(turret); //destroy old turret
    }


    public void SalvageTurret(Node node)
    {
        if (!isUpgraded)
        {
            PlayerStats.wood += turretBlueprint.salvageWood;
            PlayerStats.iron += turretBlueprint.salvageIron;
        }
        else if (isUpgraded)
        {
            PlayerStats.wood += turretBlueprint.salvageUpgradeWood;
            PlayerStats.iron += turretBlueprint.salvageUpgradeIron;
        }

        Destroy(turret); //destroy old turret
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (turret != null)
        {
            buildManager.SelectNode(this);
            return;
        }

        if (!buildManager.CanBuild)
            return;
        //build turret
        BuildTurret(buildManager.GetTurretToBuild());

    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManager.CanBuild)
            return;

        rend.material.color = canPlaceColour;

        if (turret != null || buildManager.HasMoney == false)
        {
            rend.material.color = cannotPlaceColour;
        }

    }

    void OnMouseExit()
    {
        rend.material.color = startColour; //not hovered over colour
    }
}