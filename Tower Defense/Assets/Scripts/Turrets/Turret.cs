using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private Transform target;

    [Header("Attributes")]
    public float range = 15f;
    public float fireRate = 1f;
    private float fireCountdown = 0f;

    [Header("Unity Fields")]
    public float turnSpeed = 10f;
    public Transform partToRatate;
    public string enemyTag = "Enemy";
    public GameObject ammoPrefab;
    public Transform firingPoint;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f); //repeats method 2 times a second
    }

    void UpdateTarget()
    {
        //check/set enemy target
        GameObject [] enemies = GameObject.FindGameObjectsWithTag(enemyTag); //list of enemies
        float shortestDistance = Mathf.Infinity; //if enemy not found
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position); //distance value of enemy
            //if the distance to enemy is less than the shortest distance, detect that enemy
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy; //set shortest distance to enemy
                nearestEnemy = enemy; //set this enemy 
            }
        }

        //if found enemy, set it as target
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        //else reset target to null
        else
        {
            target = null;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
            return;

        //target lock
        Vector3 dir = target.position - transform.position; //direction of enemy
            //turn towards enemy 
        Quaternion lookRotation = Quaternion.LookRotation(dir); 
        Vector3 rotation = Quaternion.Lerp(partToRatate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;//prevents snap when turning to new target
            //(rotation only on y axis)
        partToRatate.rotation = Quaternion.Euler (0f, rotation.y, 0f);

        if (fireCountdown <= 0f)//time to shoot
        {
            Shoot();
            fireCountdown = 1f/fireRate; 
        }

        fireCountdown -= Time.deltaTime;//countdown every frame
    }

    void Shoot()
    {       
        GameObject ammoGo = (GameObject)Instantiate (ammoPrefab, firingPoint.position, firingPoint.rotation);//instantiate ammo
        Ammo ammo = ammoGo.GetComponent<Ammo>();

        if (ammo != null)
        {
            ammo.Pursue(target);
        }
    }

    void OnDrawGizmosSelected()
    {
        //radius of turret
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}