# GX203_Tower Defense
**Knights vs. Space Invaders**

![Title](https://gitlab.com/Nejita/gx203_tower-defense/-/blob/main/Readme%20pics/Screenshot_2021-08-31_105920.png)

Knights vs. Space invaders is a node based infinite tower defense game. 

Protect your tower from oncoming waves by purchasing and upgrading your turrets.

Assets from https://www.kenney.nl/assets

![Gameplay](https://gitlab.com/Nejita/gx203_tower-defense/-/blob/main/Readme%20pics/Screenshot_2021-08-31_100930.png)
![Pause](https://gitlab.com/Nejita/gx203_tower-defense/-/blob/main/Readme%20pics/Screenshot_2021-08-31_101122.png)
![GameOver](https://gitlab.com/Nejita/gx203_tower-defense/-/blob/main/Readme%20pics/Screenshot_2021-08-31_101601.png)


[Download from Itch.io](https://nanaidoo.itch.io/knights-vs-aliens).
